/* global SimpleSchema */

import { ProficiencyLevelList } from '/imports/lib/AppConstants.js';

ClassSchema = new SimpleSchema({
  teacherId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  studentId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date,
    optional: true
  },
  startDate: {
    type: Date
  },
  endDate: {
    type: Date
  },
  hourValue: {
    type: Number
  },
  valid: {
    type: Boolean,
    defaultValue: true
  },
  paidAt: {
    type: Date,
    optional: true
  },
  location: {
    type: String,
    optional: true
  },
  level: {
    type: String,
    allowedValues: ProficiencyLevelList(),
    optional: true
  },
  topics: {
    type: [String],
    defaultValue: []
  },
  notes: {
    type: [String],
    defaultValue: []
  }
});

export default ClassSchema;