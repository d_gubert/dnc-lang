/* global $, jQuery */
import ReactDOM from 'react-dom';
import mask from 'vanilla-masker';

export default function maskField(field) {
  if (!field) {
    return;
  }

  let $element;

  switch (true) {
    case (field instanceof jQuery):
      $element = field;
      break;

    case (field instanceof String):
    case ("nodeType" in field):
      $element = $(field);
      break;

    case !!(field.props && field.refs && field.state && field.context):
      $element = $(ReactDOM.findDOMNode(field));
      break;
  }

  if (!$element.is('input')) {
    $element = $element.find('input');
  }

  $element.each(_maskInputEachCallback);
}

function _maskInputEachCallback() {
  if (typeof this.dataset.mask !== undefined) {
    _maskByDataSet(this, this.dataset.mask);
  } else {
    _maskByType(this);
  }
}

function _maskByDataSet(element, dataMask) {
  switch (dataMask) {
    case 'money':
      mask(element).maskMoney();
      break;

    case 'number':
      mask(element).maskNumber();
      break;

    default:
      mask(element).maskPattern(dataMask);
      break;
  }
}

function _maskByType(element) {
  switch (element.type) {
    case 'number':
      mask(element).maskNumber();
      break;
  }
}