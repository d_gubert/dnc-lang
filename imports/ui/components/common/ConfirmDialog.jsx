import React, { Component, PropTypes } from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

export default class ConfirmDialog extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      open: false,
      title: "Confirm Action",
      message: "",
      onCancel: null,
      onConfirm: null
    }
    
    this.handleClose = this.handleClose.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleConfirm = this.handleConfirm.bind(this);
    this.requestOpen = this.requestOpen.bind(this);
    
    ReactBridge.define('ConfirmDialog.open',  this.requestOpen);
    ReactBridge.define('ConfirmDialog.close', this.handleClose);
  }
  
  requestOpen({ message, title, onCancel, onConfirm }) {
    if (typeof message !== 'string') {
      throw new Error('Message has to be a string');
    }
    
    if (typeof onConfirm !== 'function') {
      throw new Error('onConfirm has to be a function');
    }
    
    this.setState({open: true, message, title, onCancel, onConfirm})
  }
  
  handleClose() {
    this.setState({open: false});
  }
  
  handleCancel() {
    if (typeof this.state.onCancel === 'function') {
      this.state.onCancel();
    }
    
    this.handleClose();
  }
  
  handleConfirm() {
    let close = true;
    
    if (typeof this.state.onConfirm === 'function') {
      close = this.state.onConfirm();
    }
    
    if (close !== false) {
      this.handleClose();
    }
  }
  
  render() {
    const actions = [
      <FlatButton label="Cancel" primary={true} onTouchTap={this.handleCancel} />,
      <FlatButton label="Confirm" primary={true} keyboardFocused={true} onTouchTap={this.handleConfirm} />
    ];
    
    return (
      <Dialog
        title={this.state.title}
        actions={actions}
        open={this.state.open}
        onRequestClose={this.handleClose}>
        
        {this.state.message}
        
      </Dialog>
    );
  }
}