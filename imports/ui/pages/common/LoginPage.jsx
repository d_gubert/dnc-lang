import { Template } from 'meteor/templating';
import { Blaze } from 'meteor/blaze';
import { Meteor } from 'meteor/meteor';

import React, { Component } from 'react';

import { Accounts } from 'meteor/accounts-base';

export default class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.goToRoot = this.goToRoot.bind(this);
  }

  componentWillMount() {
    if (Meteor.userId()) {
      this.goToRoot();
    }
  }

  componentDidMount() {
    this.view = Blaze.render(Template.atForm, this.container);
    Accounts.onLogin(this.goToRoot);
  }

  componentWillUnmount() {
    Blaze.remove(this.view);
  }

  goToRoot() {
    this.context.router.push("/");
  }

  render() {
    return (
      <span>
        <link type="text/css" rel="stylesheet" href="/css/loginPage.css" />
        <span ref={(span) => this.container = span} />
      </span>
    )
  }
}

LoginPage.contextTypes = {
  router: React.PropTypes.object.isRequired
};
