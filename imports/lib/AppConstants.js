import keyMirror from 'keymirror';

/* export a function so that nobody can change the exported array */
export const ProficiencyLevelList = () => ['kids', 'begginer', 'preIntermediate', 'intermediate', 'advanced'];

export const ProficiencyLevels = keyMirror({kids: null, beginner: null, preIntermediate: null, intermediate: null, advanced: null});