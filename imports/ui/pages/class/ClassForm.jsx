/* global $ */
import { Meteor } from 'meteor/meteor';

import { createContainer } from 'meteor/react-meteor-data';

import Students from '/imports/api/students/students.js';
import Classes from '/imports/api/classes/classes.js';

import React, { Component, PropTypes } from 'react';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';
import Checkbox from 'material-ui/Checkbox';

import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton';
import ArrowBackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import DoneIcon from 'material-ui/svg-icons/action/done';

import humanize from 'string-humanize';

import { ProficiencyLevelList } from '/imports/lib/AppConstants.js';
import maskField from '/imports/lib/ui/maskField.js';
import { toMoney  } from 'vanilla-masker';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

const doneIconStyles = {
  marginTop: '-5px',
  height: '35px',
  width: '35px'
};

class ClassForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedLevel: '',
      selectedStudent: '',
      selectedDate: null,
      selectedStart: null,
      selectedEnd: null,
      selectedHourValue: '',
      topic1: '',
      topic2: '',
      topic3: '',
      topic4: '',
      isPaid: false
    };

    this.method = 'Class.add';

    this.form = null;
    this.submitButton = null;
  }

  componentWillReceiveProps(newProps) {
    if (newProps.formIsEdit) {
      this.setState({
        selectedLevel: newProps.classData.level,
        selectedStudent: newProps.classData.studentId,
        selectedDate: newProps.classData.startDate,
        selectedStart: newProps.classData.startDate,
        selectedEnd: newProps.classData.endDate,
        selectedHourValue: newProps.classData.hourValue * 100,
        topic1: newProps.classData.topics[0],
        topic2: newProps.classData.topics[1],
        topic3: newProps.classData.topics[2],
        topic4: newProps.classData.topics[3],
        isPaid: !!newProps.classData.paidAt
      });

      this.method = 'Class.update';
    }
  }

  componentDidMount() {
    ReactBridge.call('AppBar.setLeftButton', {element: (
      <IconButton onClick={() => this.context.router.push('/classes')}>
        <ArrowBackIcon />
      </IconButton>
    )});

    ReactBridge.call('AppBar.setRightButton', {element: (
      <IconButton onClick={() => this.submitButton.click()}>
        <DoneIcon style={doneIconStyles} />
      </IconButton>
    )});
  }

  componentWillUnmount() {
    ReactBridge.call('AppBar.reset');
  }

  handleSubmit(event) {
    event.preventDefault();

    const data = $(this.form).serializeObject();

    data.level = this.state.selectedLevel;
    data.studentId = this.state.selectedStudent;
    data.classId = this.props.classData ? this.props.classData._id : undefined;
    data.topics = typeof data.topics === 'object' ? data.topics : [data.topics];

    let method = this.method;
    let router = this.context.router;

    (function classMethod() {
      Meteor.call(method, data, (error) => {
        if (typeof error !== 'undefined') {
          ReactBridge.call('Snackbar.showMessage', 'Problem saving class', {
            actionName: 'Retry',
            actionHandler: classMethod
          });
        } else {
          ReactBridge.call('Snackbar.showMessage', 'Class saved!');

          router.push('/classes');
        }
      })
    })();
  }

  render() {
    const renderLevelList = () => {
      return ProficiencyLevelList().map((level, index) =>
        <MenuItem key={index} value={level} primaryText={humanize(level)} />
      );
    };

    const renderStudents = () => {
      return this.props.students.map((student, index) =>
        <MenuItem key={index} value={student._id} primaryText={student.name} />
      );
    };

    const isPaid = this.props.classData ? typeof this.props.classData.paid !== 'undefined' : false;

    return (
      <form style={{margin: '10px', width: '95%'}} ref={(form) => this.form = form} onSubmit={this.handleSubmit.bind(this)}>
        <DatePicker
          required
          fullWidth={true}
          hintText="Class Date"
          name="date"
          value={this.state.selectedDate}
          autoOk={true}
          onChange={(event, selectedDate) => this.setState({selectedDate})} /><br />

        <TimePicker
          required
          fullWidth={true}
          format="24hr"
          hintText="Start Time"
          name="start"
          value={this.state.selectedStart}
          autoOk={true}
          onChange={(event, selectedStart) => this.setState({selectedStart})} /><br />

        <TimePicker
          required
          fullWidth={true}
          format="24hr"
          hintText="End Time"
          name="end"
          autoOk={true}
          value={this.state.selectedEnd}
          onChange={(event, selectedEnd) => this.setState({selectedEnd})} /><br />

        <SelectField
          fullWidth={true}
          floatingLabelText="Student"
          name="studentId"
          value={this.state.selectedStudent}
          onChange={(event, index, selectedStudent) => this.setState({selectedStudent})} >
          {renderStudents()}
        </SelectField>

        <SelectField
          fullWidth={true}
          floatingLabelText="Level"
          name="level"
          value={this.state.selectedLevel}
          onChange={(event, index, selectedLevel) => this.setState({selectedLevel})} >
          {renderLevelList()}
        </SelectField>

        <TextField
          required
          id="hourValue"
          type="tel"
          name="hourValue"
          data-mask="money"
          fullWidth={true}
          floatingLabelText="Hour Value"
          ref={maskField}
          value={toMoney(this.state.selectedHourValue)}
          onChange={(event, selectedHourValue) => this.setState({selectedHourValue})} /><br />

        <TextField
          fullWidth={true}
          floatingLabelText="Topic #1"
          name="topics"
          value={this.state.topic1}
          onChange={(event, topic1) => this.setState({topic1})} /><br />

        <TextField
          fullWidth={true}
          floatingLabelText="Topic #2"
          name="topics"
          value={this.state.topic2}
          onChange={(event, topic2) => this.setState({topic2})} /><br />

        <TextField
          fullWidth={true}
          floatingLabelText="Topic #3"
          name="topics"
          value={this.state.topic3}
          onChange={(event, topic3) => this.setState({topic3})} /><br />

        <TextField
          fullWidth={true}
          floatingLabelText="Topic #4"
          name="topics"
          value={this.state.topic4}
          onChange={(event, topic4) => this.setState({topic4})} /><br />

        <Checkbox
          label="Class has been paid"
          name="paid"
          checked={this.state.isPaid}
          onCheck={(event, isPaid) => { console.log(isPaid); this.setState({isPaid}) }} /><br /><br />

        <input type="submit" style={{display: 'none'}} ref={(submitButton) => this.submitButton = submitButton} />
      </form>
    );
  }
}

ClassForm.contextTypes = {
  router: PropTypes.object.isRequired
};

export default createContainer(({ params }) => {
   let classData = {}, formIsEdit = false;

  if (params.id) {
    Meteor.subscribe('Classes.One', params.id);
    classData = Classes.findOne(params.id);
    formIsEdit = typeof classData !== 'undefined';
  }

  return {
    students: Students.find().fetch() || [],
    classData,
    formIsEdit
  }
}, ClassForm);
