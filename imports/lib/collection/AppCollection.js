import { Mongo } from 'meteor/mongo';

import { mix } from '/imports/lib/helper/mixwith.js';

import HookableCollectionMixin from './HookableCollectionMixin.js';
import SchemafulCollectionMixin from './SchemafulCollectionMixin.js';
import DateControlledCollectionMixin from './DateControlledCollectionMixin.js';

AppCollection = mix(Mongo.Collection).with(SchemafulCollectionMixin, DateControlledCollectionMixin, HookableCollectionMixin);

export default AppCollection; 