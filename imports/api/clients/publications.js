import { Meteor } from 'meteor/meteor';

import Clients from './clients.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.publish('Clients.ListActive', function() {
  return Clients.find({active: true, teacherId}, {sort: {name: 1}, fields: {name:1, email: 1, hourValue: 1}})
});

Meteor.publish('Clients.One', function(_id) {
  return Clients.find({_id, teacherId});
});