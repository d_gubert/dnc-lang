import '/imports/api/clients/methods.js';
import '/imports/api/students/methods.js';
import '/imports/api/classes/methods.js';

import '/imports/api/clients/publications.js';
import '/imports/api/students/publications.js';
import '/imports/api/classes/publications.js';

import { fs } from 'meteor/peerlibrary:fs';

const requireApiPublications = (apiPath, requirePath = '/imports/api/') => {
  fs.readdirSync(apiPath).map((item) => {
    if (item === 'publications.js') {
      require(requirePath+item);
    } else if (fs.lstatSync(apiPath+item).isDirectory()) {
      requireApiPublications(apiPath+item+'/', requirePath+item+'/');
    }
  });
}

// requireApiPublications('/home/douglas/dev/meteor/dnc-lang/imports/api/');