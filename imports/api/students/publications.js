import { Meteor } from 'meteor/meteor';

import Students from './students.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.publish('Students.ListActive', function() {
  return Students.find({active: true, teacherId}, {sort: {name: 1}, fields: {name: 1, level: 1}});
});

Meteor.publish('Students.One', function(_id) {
  return Students.find({_id, teacherId});
});