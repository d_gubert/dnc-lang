import React, { Component, PropTypes } from 'react';

import { createContainer } from 'meteor/react-meteor-data';

import { Meteor } from 'meteor/meteor';

import humanize from 'string-humanize';
import { toPattern, toMoney } from 'vanilla-masker';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';
import DetailAppBarComponent from '/imports/ui/components/DetailAppBarComponent.jsx';

import Clients from '/imports/api/clients/clients.js';

class ClientDetail extends DetailAppBarComponent {
  constructor(props) {
    super(props, {
      back: '/clients',
      dialogMessage: 'Are you sure you want to delete this client?'
    });

    this.deleteCallback = this.deleteCallback.bind(this);
  }

  deleteCallback() {
    Meteor.call('Client.inactivate', this.props.client._id, (error, result) => {
      ReactBridge.call('ConfirmDialog.close');

      if (typeof error !== 'undefined') {
        ReactBridge.call('Snackbar.showMessage', 'Client not deleted due to server error', {
          actionName: 'Retry',
          actionHandler: this.deleteCallback
        });
      } else {
        ReactBridge.call('Snackbar.showMessage', 'Client deleted successfully');
        this.context.router.push('/clients');
      }

      this.deleteIsRunning = false;
    });

    return false;
  }

  render() {
    const { name, email } = this.props.client;

    const phone = this.props.client.phone || '';
    const hourValue = this.props.client.hourValue || '';

    return (
      <section>
        <div className="row">
          <span>Name: </span>
          <span>{name}</span>
        </div>
        <div className="row">
          <span>Email: </span>
          <span>{email}</span>
        </div>
        <div className="row">
          <span>Phone: </span>
          <span>{toPattern(phone, {pattern: '(99) 9999-9999'})}</span>
        </div>
        <div className="row">
          <span>Hour Value: </span>
          <span>{toMoney(hourValue)}</span>
        </div>
      </section>
    );
  }
}

ClientDetail.contextTypes = {
  router: PropTypes.object.isRequired
};

ClientDetail.propTypes = {
  client: PropTypes.object.isRequired
};

export default createContainer(({ params }) => {
  const { id } = params;

  Meteor.subscribe('Clients.One', id);

  return {
    client: Clients.findOne(id) || {}
  }
}, ClientDetail);
