import React, { Component, PropTypes } from 'react';

import humanize from 'string-humanize';

import { Table, TableHeader, TableBody, TableRow, TableHeaderColumn, TableRowColumn }
  from 'material-ui/Table';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import AddIcon from 'material-ui/svg-icons/content/add';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

const floatingActionButtonStyle = {
  bottom: "10px",
  right: "20px",
  position: "fixed"
};

export default class TableListTemplate extends Component {
  componentDidMount() {
    ReactBridge.call('AppBar.setTitle', this.props.title);
  }

  componentDidUpdate() {
    ReactBridge.call('AppBar.setTitle', this.props.title);
  }

  renderItems() {
    this.mapRowIndexToId = [];
    return this.props.items.map((item, index) => {
      this.mapRowIndexToId[index] = item._id;

      return (
        <TableRow key={item._id}>
          {this.props.fields.map((field, index) => (
            <TableRowColumn key={item._id + index}>{item[field]}</TableRowColumn>
          ))}
        </TableRow>
      );
    });
  }

  handleRowSelection([ rowIndex ]) {
    if (typeof this.mapRowIndexToId[rowIndex] !== 'string') return false;

    this.context.router.push(`/${this.props.pathName}/${this.mapRowIndexToId[rowIndex]}`);
  }

  render() {
    return (
      <section>
        <Table className="hideTableFirstColumn" onRowSelection={this.handleRowSelection.bind(this)}>
          <TableHeader adjustForCheckbox={false} displaySelectAll={false}>
            <TableRow>
              {this.props.fields.map((field, index) => (
                <TableHeaderColumn key={this.props.pathName+'_'+field}>{humanize(field)}</TableHeaderColumn>
              ))}
            </TableRow>
          </TableHeader>
          <TableBody showRowHover={true} displayRowCheckbox={false} >
            {this.renderItems()}
          </TableBody>
        </Table>
        <FloatingActionButton
          onClick={() => this.context.router.push(`/${this.props.pathName}/add`)}
          style={floatingActionButtonStyle}>
          <AddIcon />
        </FloatingActionButton>
      </section>
    );
  }
}

TableListTemplate.contextTypes = {
  router: PropTypes.object.isRequired
};

TableListTemplate.propTypes = {
  items: PropTypes.array.isRequired,
  pathName: PropTypes.string.isRequired,
  fields: PropTypes.array.isRequired,
  title: PropTypes.string
};
