import React, { Component } from 'react';

import MaterialSnackbar from 'material-ui/Snackbar';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

export default class Snackbar extends Component {
  constructor(props) {
    super(props);

    this.defaultValues = {
      bodyStyle: {textAlign: 'center'},
      autoHideDuration: 2000
    }

    this.state = {
      open: false,
      action: null,
      message: '',
      bodyStyle: this.defaultValues.bodyStyle,
      autoHideDuration: this.defaultValues.autoHideDuration,
      onActionTouchTap: null
    }

    ReactBridge.define('Snackbar.showMessage', this.showMessage.bind(this));
  }

  showMessage(message, actionOptions = {}) {
    if (typeof message !== 'string') {
      throw new Error('Snackbar message has to be a string');
    }

    const state = {
      open: true,
      message
    };

    if (actionOptions.actionName && typeof actionOptions.actionHandler === 'function') {
      state.bodyStyle = {};
      state.action = actionOptions.actionName;
      state.onActionTouchTap = actionOptions.actionHandler;
    } else {
      state.bodyStyle = this.defaultValues.bodyStyle;
      state.autoHideDuration = this.defaultValues.autoHideDuration;
    }

    this.setState(state);
  }

  render() {
    return (
      <MaterialSnackbar
        message={this.state.message}
        open={this.state.open}
        autoHideDuration={this.state.autoHideDuration}
        bodyStyle={this.state.bodyStyle}
        action={this.state.action}
        onActionTouchTap={this.state.onActionTouchTap}
        onRequestClose={() => this.setState({open: false})}
      />
    );
  }
}
