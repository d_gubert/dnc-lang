import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import * as Sanitizer from '/imports/lib/server/sanitizeInput.js';

import Students from './students.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.methods({
  'Student.add'(data) {
    check(data, {
      name: String,
      email: String,
      hourValue: String,
      phone: Match.Maybe(String),
      level: Match.Maybe(String),
      isClient: Match.Maybe(String)
    });

    // @TODO Only teachers can add students

    let clientId;

    if (data.isClient) {
      clientId = Meteor.call('Client.add', {
        name: data.name,
        email: data.email,
        phone: data.phone,
        hourValue: data.hourValue
      });
    }

    data.hourValue = Sanitizer.sanitizeNumber(data.hourValue);
    data.phone = Sanitizer.sanitizePhone(data.phone);

    return Students.insert({
      name: data.name,
      email: data.email,
      phone: data.phone,
      level: data.level,
      teacherId,
      clientId
    });
  },
  
  'Student.inactivate'(_id) {
    check(_id, String);
    
    Students.update({_id}, {$set: {active: false}});
  }
});