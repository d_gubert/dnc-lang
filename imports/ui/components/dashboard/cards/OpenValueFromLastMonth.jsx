import React, { Component, PropTypes } from 'react';

import { Meteor } from 'meteor/meteor';

import { Card, CardText } from 'material-ui/Card';

import moment from 'moment';

import Classes from '/imports/api/classes/classes';

export default class OpenValueFromLastMonth extends Component {
  constructor() {
    super();

    this.state = {
      lastMonthSum: 0
    };

    this.trackerComputation = null;
    this.classMap = new Map;

    this.addToLastMonthSum = this.addToLastMonthSum.bind(this);
  }

  addToLastMonthSum(id, value) {
    this.classMap.set(id, value);

    this.setState((state) => {
      return {lastMonthSum: state.lastMonthSum + value}
    });
  }

  componentWillMount() {
    this.trackerComputation = Tracker.autorun(() => {
      const lastDayOfPreviousMonth = moment()
        .startOf('month')
        .subtract(1, 'day')
        .endOf('day');

      const firstDayOfLastMonth = moment(lastDayOfPreviousMonth)
        .startOf('month')
        .startOf('day');

      Classes.find({
        // teacherId: Meteor.userId(),
        startDate: {
          $gte: firstDayOfLastMonth.toDate(),
          $lte: lastDayOfPreviousMonth.toDate()
        }
      }).observeChanges({
        added: (id, fields) => {
          if (this.classMap.has(id)) {
            return;
          }

          this.addToLastMonthSum(id, fields.hourValue);
        },

        changed: (id, fields) => {
          if (!fields.hourValue) {
            return;
          }

          const valueDiff = fields.hourValue - this.classMap.get(id);

          this.addToLastMonthSum(id, valueDiff);
        },

        removed: (id) => {
          if (!this.classMap.has(id)) {
            return;
          }

          const valueDiff = 0 - this.classMap.get(id);

          this.addToLastMonthSum(id, valueDiff);
        }
      });
    })
  }

  componentWillUnmount() {
    if (this.trackerComputation !== null) {
      this.trackerComputation.stop();
    }
  }

  render() {
    return (
      <Card>
        <CardText>
          {this.state.lastMonthSum}
        </CardText>
      </Card>
    );
  }
}
