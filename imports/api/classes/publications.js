import { Meteor } from 'meteor/meteor';

import Classes from './classes.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.publish('Classes.ListValid', function() {
  return Classes.find(
    {valid: true, teacherId},
    {
      sort: {startDate: -1},
      fields: {topics: 0, notes: 0, createdAt: 0, updatedAt: 0}
    });
});

Meteor.publish('Classes.One', function(_id) {
  return Classes.find({_id, teacherId});
});
