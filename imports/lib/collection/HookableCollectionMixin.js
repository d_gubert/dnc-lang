const HookableCollectionMixin = (Collection) => class extends Collection {
  insert(doc, callback) {
    let retHook, retInsert;

    if (typeof this.beforeInsert === 'function') {
      retHook = this.beforeInsert(doc, callback);

      if (retHook === false) {
        return;
      }
    }

    retInsert = super.insert(doc, callback);

    if (typeof this.afterInsert === 'function') {
      this.afterInsert(retInsert, doc, callback);
    }

    return retInsert;
  }
}

export default HookableCollectionMixin;