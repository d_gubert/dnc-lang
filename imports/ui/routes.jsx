import { Meteor } from 'meteor/meteor';

import React from 'react';
import { Router, Route, Redirect, browserHistory, createMemoryHistory } from 'react-router';

import App from './App.jsx';

import DashboardLayout from './layouts/DashboardLayout.jsx';

import Dashboard from './pages/Dashboard';

import StudentList from './pages/student/StudentList.jsx';
import StudentForm from './pages/student/StudentForm.jsx';
import StudentDetail from './pages/student/StudentDetail.jsx';

import ClientList from './pages/client/ClientList.jsx';
import ClientDetail from './pages/client/ClientDetail.jsx';

import ClassList from './pages/class/ClassList.jsx';
import ClassForm from './pages/class/ClassForm.jsx';

import LoginPage from './pages/common/LoginPage.jsx';

let history;

if (Meteor.isClient) {
  history = browserHistory;
} else {
  history = createMemoryHistory('/');
}

export default (
  <Router history={history}>
    <Route component={App}>
      <Route name="Login" path="/login" component={LoginPage} />

      <Route component={DashboardLayout}>

        <Redirect from="/" to="/dashboard" />
        <Route path="/dashboard" component={Dashboard} />

        <Route path="/students" component={StudentList} />
        <Route path="/clients"  component={ClientList} />
        <Route path="/classes"  component={ClassList} />

        <Route path="/student">
          <Route path="add" component={StudentForm} />
          <Route path=":id" component={StudentDetail} />
        </Route>

        <Route path="/client">
          <Route path=":id" component={ClientDetail} />
        </Route>

        <Route path="/class">
          <Route path="add" component={ClassForm} />
          <Route path=":id" component={ClassForm} />
        </Route>
      </Route>
    </Route>
  </Router>
);
