import moment from 'moment';
import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import * as Sanitizer from '/imports/lib/server/sanitizeInput.js';

import Classes from './classes.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.methods({
  'Class.add'(data) {
    check(data, {
      date: String,
      start: String,
      end: String,
      studentId: String,
      level: String,
      hourValue: String,
      paid: Match.Maybe(String),
      topics: Match.Maybe([String]),
      classId: Match.Maybe(undefined)
    });

    const classDoc = {
      startDate: moment(`${data.date}T${data.start}:00-03:00`).toDate(),
      endDate: moment(`${data.date}T${data.end}:00-03:00`).toDate(),
      hourValue: Sanitizer.sanitizeNumber(data.hourValue),
      studentId: data.studentId,
      level: data.level,
      topics: data.topics,
      teacherId
    };

    if (data.paid) {
      classDoc.paidAt = new Date();
    }

    return Classes.insert(classDoc);
  },

  'Class.update'(data) {
    check(data, {
      classId: String,
      date: String,
      start: String,
      end: String,
      studentId: String,
      level: String,
      hourValue: String,
      paid: Match.Maybe(String),
      topics: Match.Maybe([String])
    });

    const classDoc = {
      startDate: moment(`${data.date}T${data.start}:00-03:00`).toDate(),
      endDate: moment(`${data.date}T${data.end}:00-03:00`).toDate(),
      hourValue: Sanitizer.sanitizeNumber(data.hourValue),
      studentId: data.studentId,
      level: data.level,
      topics: data.topics,
      teacherId
    };

    if (data.paid) {
      classDoc.paidAt = new Date();
    } else {
      classDoc.paidAt = null;
    }

    return Classes.update({_id: data.classId, teacherId}, {$set: classDoc});
  },

  'Class.invalidate'(_id) {
    check(_id, String);

    Classes.update({_id, teacherId}, {$set: {valid: false}});
  }
});
