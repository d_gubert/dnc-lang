/* global SimpleSchema */

TeacherSchema = new SimpleSchema({
  name: {
    type: String
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  phone: {
    type: Number,
    optional: true
  }
});

export default TeacherSchema;