import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';

import Routes from '/imports/ui/routes.jsx';

Meteor.startup(() => {
  Session.set('teacherId', '4cyFaamkXrMbsxZNx');

  render(Routes, document.getElementById('App'));
});