import AppCollection from '/imports/lib/collection/AppCollection.js';
import ClientSchema from './schema.js';

import Students from '/imports/api/students/students.js';

const Clients = new AppCollection('clients', ClientSchema);

export default Clients;