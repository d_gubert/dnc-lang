/* global SimpleSchema */

const SchemafulCollectionMixin = (Collection) => class extends Collection {
  constructor(collectionName, simpleSchemaInstance, options = {}) {
    super(collectionName, options);

    if (simpleSchemaInstance instanceof SimpleSchema) {
      this.attachSchema(simpleSchemaInstance);
    }
  }
}

export default SchemafulCollectionMixin;