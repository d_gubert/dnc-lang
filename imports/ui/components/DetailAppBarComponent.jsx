import React, { Component, PropTypes } from 'react';

import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import ArrowBackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

export default class DetailAppBarComponent extends Component {
  constructor(props, appBarOptions) {
    super(props);

    if (typeof appBarOptions.back !== 'string' || !appBarOptions.back) {
      throw new Error('You must provide a back link to the App Bar');
    }

    this.appBarOptions = appBarOptions;

    this.deleteIsRunning = false;

    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    ReactBridge.call('AppBar.setLeftButton', {element: (
      <IconButton onClick={() => this.context.router.push(this.appBarOptions.back)}>
        <ArrowBackIcon />
      </IconButton>
    )});

    ReactBridge.call('AppBar.setRightButton', {element: (
      <IconMenu
        iconButtonElement={
          <IconButton><MoreVertIcon /></IconButton>
        }
        targetOrigin={{horizontal: 'right', vertical: 'top'}}
        anchorOrigin={{horizontal: 'right', vertical: 'top'}}>

        <MenuItem primaryText="Edit" />
        <MenuItem primaryText="Delete" onTouchTap={this.handleDelete} />

      </IconMenu>
    )});
  }

  componentWillUnmount() {
    ReactBridge.call('AppBar.reset');
  }

  handleDelete(event) {
    event.stopPropagation();
    // For some reason, the event is being triggered twice
    // So I had to add the little hack
    if (this.deleteIsRunning) {
      return;
    }

    this.deleteIsRunning = true;

    const message = this.appBarOptions.dialogMessage || "Are you sure you want to delete this document?";

    ReactBridge.call('ConfirmDialog.open', {
      onConfirm: this.deleteCallback,
      onCancel: () => {this.deleteIsRunning = false;},
      message
    });
  }
}

DetailAppBarComponent.contextTypes = {
  router: React.PropTypes.object.isRequired
};

DetailAppBarComponent.propTypes = {
  back: PropTypes.string.isRequired,
  dialogMessage: PropTypes.string
};
