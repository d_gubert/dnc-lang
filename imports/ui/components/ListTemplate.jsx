import React, { Component, PropTypes } from 'react';
import humanize from 'string-humanize';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import AddIcon from 'material-ui/svg-icons/content/add';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

const floatingActionButtonStyle = {
  bottom: "10px",
  right: "20px",
  position: "fixed"
};

