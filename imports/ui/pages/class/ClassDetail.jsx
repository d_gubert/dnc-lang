import React, { Component, PropTypes } from 'react';

import { createContainer } from 'meteor/react-meteor-data';

import { Meteor } from 'meteor/meteor';

import humanize from 'string-humanize';
import { toPattern, toMoney } from 'vanilla-masker';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';
import DetailAppBarComponent from '/imports/ui/components/DetailAppBarComponent.jsx';

import Students from '/imports/api/students/students.js';

class StudentDetail extends DetailAppBarComponent {
  constructor(props) {
    super(props, {
      back: '/students',
      dialogMessage: 'Are you sure you want to delete this student?'
    });

    this.deleteCallback = this.deleteCallback.bind(this);
  }

  deleteCallback() {
    Meteor.call('Class.invalidate', this.props.student._id, (error, result) => {
      ReactBridge.call('ConfirmDialog.close');

      if (typeof error !== 'undefined') {
        ReactBridge.call('Snackbar.showMessage', 'Student not deleted due to server error');
      } else {
        ReactBridge.call('Snackbar.showMessage', 'Student deleted successfully');
        this.context.router.push('/students');
      }

      this.deleteIsRunning = false;
    });

    return false;
  }

  renderClientInfo() {
    if (!this.props.student.clientInfo) {
      return '';
    }

    const { hourValue } = this.props.student.clientInfo;

    return (
      <section>
        <div className="row">
          <span>Hour Value: </span>
          <span>{toMoney(hourValue)}</span>
        </div>
      </section>
    );
  }

  render() {
    const { name, email, level } = this.props.student;

    const phone = this.props.student.phone || '';

    return (
      <section>
        <div className="row">
          <span>Name: </span>
          <span>{name}</span>
        </div>
        <div className="row">
          <span>Email: </span>
          <span>{email}</span>
        </div>
        <div className="row">
          <span>Level: </span>
          <span>{humanize(level)}</span>
        </div>
        <div className="row">
          <span>Phone: </span>
          <span>{toPattern(phone, {pattern: '(99) 9999-9999'})}</span>
        </div>
        {this.renderClientInfo()}
      </section>
    );
  }
}



export default createContainer(({ params }) => {
  const { id } = params;

  Meteor.subscribe('Students.One', id);

  return {
    student: Students.findStudentWithClientInfo(id) || {}
  }
}, StudentDetail);
