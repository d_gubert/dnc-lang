/* global SimpleSchema */

import { ProficiencyLevelList } from '/imports/lib/AppConstants.js';

StudentSchema = new SimpleSchema({
  name: {
    type: String,
    max: 200
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  teacherId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date,
    optional: true
  },
  clientId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id,
    optional: true
  },
  phone: {
    type: Number,
    optional: true
  },
  level: {
    type: String,
    allowedValues: ProficiencyLevelList(),
    optional: true
  },
  active: {
    type: Boolean,
    defaultValue: true
  },
  grades: {
    type: [Object],
    defaultValue: []
  },
  'grades.$.speakingListeningGrade': {
    type: Number,
    decimal: true,
    min: 0,
    max: 10
  },
  'grades.$.readingWritingGrade': {
    type: Number,
    decimal: true,
    min: 0,
    max: 10
  },
  'grades.$.testDate': {
    type: Date,
    optional: true
  },
  notes: {
    type: [String],
    defaultValue: []
  }
});

export default StudentSchema;