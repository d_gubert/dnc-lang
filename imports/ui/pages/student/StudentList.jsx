import Students from '/imports/api/students/students.js';

import { createContainer } from 'meteor/react-meteor-data';

import TableListTemplate from '/imports/ui/components/TableListTemplate.jsx';

export default createContainer(() => {
  let students = Students.find().fetch() || {};
  
  return {
    items: students,
    pathName: 'student',
    fields: ['name', 'level'],
    title: 'Students'
  };
}, TableListTemplate);