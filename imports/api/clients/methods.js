import { Meteor } from 'meteor/meteor';
import { check, Match } from 'meteor/check';
import * as Sanitizer from '/imports/lib/server/sanitizeInput.js';

import Clients from './clients.js';

const teacherId = '4cyFaamkXrMbsxZNx';

Meteor.methods({
  'Client.add'(data) {
    check(data, {
      name: String,
      email: String,
      hourValue: String,
      phone: Match.Maybe(String)
    });

    data.hourValue = Sanitizer.sanitizeNumber(data.hourValue);
    data.phone = Sanitizer.sanitizePhone(data.phone);

    data.hourValueHistory = [{value: data.hourValue, date: new Date()}];

    return Clients.insert({
      name: data.name,
      email: data.email,
      hourValue: data.hourValue,
      phone: data.phone,
      hourValueHistory: data.hourValueHistory,
      teacherId
    });
  },
  
  'Client.inactivate'(_id) {
    check(_id, String);
    
    Clients.update({_id}, {$set: {active: false}});
  }
});