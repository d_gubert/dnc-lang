import React, { Component } from 'react';

import injectTapEventPlugin from 'react-tap-event-plugin';

import MaterialAppBar from 'material-ui/AppBar';
import IconButton from 'material-ui/IconButton';
import MenuIcon from 'material-ui/svg-icons/navigation/menu';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

injectTapEventPlugin();

export default class AppBar extends Component {
  constructor(props) {
    super(props);

    this.defaultLeftButton = {
      element: (
        <IconButton onClick={() => ReactBridge.call('LeftNav.toggle')}>
          <MenuIcon />
        </IconButton>
      ),
      action: null
    };

    this.defaultRightButton = {
      element: null,
      action: null
    };

    this.state = {
      title: "D&C Lang",
      leftButton: this.defaultLeftButton,
      rightButton: this.defaultRightButton
    };

    ReactBridge.define('AppBar.reset', this.resetAppBar.bind(this));
    ReactBridge.define('AppBar.setTitle', this.setTitle.bind(this));
    ReactBridge.define('AppBar.setLeftButton', this.setLeftButton.bind(this));
    ReactBridge.define('AppBar.setRightButton', this.setRightButton.bind(this));
  }

  setLeftButton({ element, action }) {
    this.setState({leftButton: {element, action}});
  }

  setRightButton({ element, action }) {
    this.setState({rightButton: {element, action}});
  }

  setTitle(title) {
    if (typeof title !== 'string'){
      throw new Error('Title has to be a string');
    }

    if (title === '') {
      title = 'D&C Lang';
    }

    this.setState({title});
  }

  resetLeftButton() {
    this.setState({leftButton: this.defaultLeftButton});
  }

  resetRightButton() {
    this.setState({rightButton: this.defaultRightButton});
  }

  resetAppBar() {
    this.resetLeftButton();
    this.resetRightButton();
    this.setTitle();
  }

  render() {
    return (
      <MaterialAppBar
        title={this.state.title}
        iconElementLeft={this.state.leftButton.element}
        iconElementRight={this.state.rightButton.element}
      />
    );
  }
};
