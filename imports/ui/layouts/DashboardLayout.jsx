import { Meteor } from 'meteor/meteor';

import React, { Component } from 'react';

import EasyTransition from 'react-easy-transition';

import AppBar from '../components/common/AppBar.jsx';
import LeftNav from '../components/common/LeftNav.jsx';

export default class DashboardLayout extends Component {
  render() {
    let children;

    if (Meteor.isCordova) {
      children = this.props.children;
    } else {
      children = (
        <EasyTransition
          path={this.props.location.pathname}
          initialStyle={{ opacity: 0 }}
          transition="0.1s ease-in"
          finalStyle={{ opacity: 1 }}>
          {this.props.children}
        </EasyTransition>
      );
    }
    return (
      <section>
        <AppBar />
        <main>
          <LeftNav />
          <section id="layoutContainer">
            {children}
          </section>
        </main>
      </section>
    );
  }
}
