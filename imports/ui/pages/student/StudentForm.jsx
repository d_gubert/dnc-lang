/* global $ */
import { Meteor } from 'meteor/meteor';

import React, { Component, PropTypes } from 'react';

import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Checkbox from 'material-ui/Checkbox';
import IconButton from 'material-ui/IconButton';
import ArrowBackIcon from 'material-ui/svg-icons/navigation/arrow-back';
import DoneIcon from 'material-ui/svg-icons/action/done';

import humanize from 'string-humanize';

import { ProficiencyLevelList } from '/imports/lib/AppConstants.js';
import maskField from '/imports/lib/ui/maskField.js';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

const doneIconStyles = {
  marginTop: '-5px',
  height: '35px',
  width: '35px'
};

export default class StudentForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedLevel: null
    };

    this.form = null;
    this.submitButton = null;
  }

  componentDidMount() {
    ReactBridge.call('AppBar.setLeftButton', {element: (
      <IconButton onClick={() => this.context.router.push('/students')}>
        <ArrowBackIcon />
      </IconButton>
    )});

    ReactBridge.call('AppBar.setRightButton', {element: (
      <IconButton onClick={() => this.submitButton.click()}>
        <DoneIcon style={doneIconStyles} />
      </IconButton>
    )});
  }

  componentWillUnmount() {
    ReactBridge.call('AppBar.reset');
  }

  handleSubmit(event) {
    event.preventDefault();
    const data = $(this.form).serializeObject();
    data.level = this.state.selectedLevel;

    Meteor.call('Student.add', data, (error, result) => {
      // @TODO add some error handling
      if (typeof error !== 'undefined') return false;

      this.context.router.push('/students');

      ReactBridge.call('Snackbar.showMessage', 'Student added!');
    });
  }

  render() {
    const renderLevelList = () => {
      return ProficiencyLevelList().map((level, index) => <MenuItem key={index} value={level} primaryText={humanize(level)} />);
    };

    return (
      <form style={{margin: '10px', width: '95%'}} ref={(form) => this.form = form} onSubmit={this.handleSubmit.bind(this)}>
        <TextField fullWidth={true} floatingLabelText="Name" type="text" name="name" required /><br />
        <TextField fullWidth={true} floatingLabelText="Email" type="email" name="email" required /><br />
        <TextField fullWidth={true} floatingLabelText="Phone" type="tel" name="phone" data-mask="(99) 9999-9999" ref={maskField} /><br />
        <TextField fullWidth={true} floatingLabelText="Hour Value" type="tel" name="hourValue" data-mask="money" ref={maskField} required /><br />
        <SelectField fullWidth={true} floatingLabelText="Level" name="level" value={this.state.selectedLevel} onChange={(event, index, selectedLevel) => this.setState({selectedLevel})}>
          {renderLevelList()}
        </SelectField>
        <Checkbox label="Student is client" defaultChecked={true} name="isClient" /><br /><br />
        <input type="submit" style={{display: 'none'}} ref={(submitButton) => this.submitButton = submitButton} />
      </form>
    );
  }
}

StudentForm.contextTypes = {
  router: PropTypes.object.isRequired
};
