  const _cachedApplicationRef = Symbol('_cachedApplicationRef');

  const _mixinRef = Symbol('_mixinRef');

  const _originalMixin = Symbol('_originalMixin');

  const wrap = (mixin, wrapper) => {
    Object.setPrototypeOf(wrapper, mixin);
    if (!mixin[_originalMixin]) {
      mixin[_originalMixin] = mixin;
    }
    return wrapper;
  };

  const Cached = mixin => wrap(mixin, superclass => {
    // Get or create a symbol used to look up a previous application of mixin
    // to the class. This symbol is unique per mixin definition, so a class will have N
    // applicationRefs if it has had N mixins applied to it. A mixin will have
    // exactly one _cachedApplicationRef used to store its applications.
    let applicationRef = mixin[_cachedApplicationRef];
    if (!applicationRef) {
      applicationRef = mixin[_cachedApplicationRef] = Symbol(mixin.name);
    }
    // Look up an existing application of `mixin` to `c`, return it if found.
    if (superclass.hasOwnProperty(applicationRef)) {
      return superclass[applicationRef];
    }
    // Apply the mixin
    let application = mixin(superclass);
    // Cache the mixin application on the superclass
    superclass[applicationRef] = application;
    return application;
  });

  const HasInstance = mixin => {
    if (Symbol.hasInstance && !mixin.hasOwnProperty(Symbol.hasInstance)) {
      Object.defineProperty(mixin, Symbol.hasInstance, {
        value: function (o) {
          const originalMixin = this[_originalMixin];
          while (o !== null) {
            if (o.hasOwnProperty(_mixinRef) && o[_mixinRef] === originalMixin) {
              return true;
            }
            o = Object.getPrototypeOf(o);
          }
          return false;
        }
      });
    }
    return mixin;
  };

  const BareMixin = mixin => wrap(mixin, superclass => {
    // Apply the mixin
    let application = mixin(superclass);

    // Attach a reference from mixin applition to wrapped mixin for RTTI
    // mixin[@@hasInstance] should use this.
    application.prototype[_mixinRef] = mixin[_originalMixin];
    return application;
  });

  const Mixin = mixin => Cached(HasInstance(BareMixin(mixin)));

  const mix = superClass => new MixinBuilder(superClass);

  class MixinBuilder {
    constructor(superclass) {
      this.superclass = superclass;
    }

    with() {
      return Array.from(arguments).reduce((c, m) => m(c), this.superclass);
    }

  }
  
  export { mix };