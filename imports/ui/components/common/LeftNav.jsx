import React, { Component, PropTypes } from 'react';

import MaterialLeftNav from 'material-ui/Drawer';
import AppBar from 'material-ui/AppBar';
import MenuItem from 'material-ui/MenuItem';
import PersonIcon from 'material-ui/svg-icons/social/person';
import ContactsIcon from 'material-ui/svg-icons/communication/contacts';
import ClassIcon from 'material-ui/svg-icons/action/class';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

export default class LeftNav extends Component {
  constructor(props) {
    super(props);
    this.state = {open: false};

    ReactBridge.define('LeftNav.toggle', this.toggleLeftNav.bind(this));
    ReactBridge.define('LeftNav.hide',   () => this.setState({open: false}));
  }

  toggleLeftNav() {
    this.setState({open: !this.state.open});
  }

  handleClick(path) {
    this.context.router.push(path);
    this.setState({open: false});
  }

  render() {
    return (
      <MaterialLeftNav
        docked={false}
        open={this.state.open}
        width={300}
        onRequestChange={open => this.setState({open})}>

        <AppBar title="Modules" showMenuIconButton={false} />

        <MenuItem leftIcon={<PersonIcon />} onClick={this.handleClick.bind(this, '/students')}>Students</MenuItem>
        <MenuItem leftIcon={<ContactsIcon />} onClick={this.handleClick.bind(this, '/clients')}>Clients</MenuItem>
        <MenuItem leftIcon={<ClassIcon />} onClick={this.handleClick.bind(this, '/classes')}>Classes</MenuItem>

      </MaterialLeftNav>
    );
  }
}

LeftNav.contextTypes = {
  router: PropTypes.object.isRequired
};
