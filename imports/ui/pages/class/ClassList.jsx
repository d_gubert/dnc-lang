import Classes from '/imports/api/classes/classes.js';

import { createContainer } from 'meteor/react-meteor-data';

import TableListTemplate from '/imports/ui/components/TableListTemplate.jsx';

export default createContainer(() => {
  let classes = Classes.find().fetch() || [];

  return {
    items: classes,
    pathName: 'class',
    fields: ['date', 'studentName'],
    title: 'Classes'
  };
}, TableListTemplate);