/* global jQuery */
(function ($) {
    $.fn.serializeObject = function () {
        const ret = {};

        this.serializeArray().forEach((value) => {
          if (ret[value.name]) {
            if (typeof ret[value.name].push !== 'function') {
              ret[value.name] = [ret[value.name]];
            }

            ret[value.name].push(value.value || '');
          } else {
            ret[value.name] = value.value || '';
          }
        });

        return ret;
    };
})(jQuery);