import Clients from '/imports/api/clients/clients.js';

import { createContainer } from 'meteor/react-meteor-data';

import TableListTemplate from '/imports/ui/components/TableListTemplate.jsx';

export default createContainer(() => {
  clients = Clients.find().fetch() || {};
  
  return {
    items: clients,
    pathName: 'client',
    fields: ['name', 'email'],
    title: 'Clients'
  };
}, TableListTemplate);