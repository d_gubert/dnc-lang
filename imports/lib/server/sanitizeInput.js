// WARNING works only with brazilian money formatting
export function sanitizeNumber(number) {
  if (typeof number !== 'string') {
    return number;
  }

  return parseFloat((number.replace(/,/g, '.')).replace(/[^0-9.\-+]/g, ''));
}

export function sanitizePhone(phone) {
  if (typeof phone !== 'string') {
    return phone;
  }

  return phone.replace(/[^0-9]/g, '');
}