import AppCollection from '/imports/lib/collection/AppCollection.js';
import TeacherSchema from './schema.js';

const Teachers = new AppCollection('teachers', TeacherSchema);

export default Teachers;