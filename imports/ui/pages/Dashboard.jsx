import React, { Component } from 'react';

import Cards from '/imports/ui/components/dashboard/cards';

export default class Dashboard extends Component {
  render() {
    return (
      <section>
        {Cards.map(Card => <Card key={Math.random()} />)}
      </section>
    )
  }
}
