const DateControlledCollectionMixin = (Collection) => class extends Collection {
  insert(doc, callback) {
    doc.createdAt = doc.createdAt || new Date();
    return super.insert(doc, callback);
  }
  
  update(selector, modifier, options, callback) {
    const $set = modifier.$set || {};
    
    $set.updatedAt = $set.updatedAt || new Date();
    return super.update(selector, modifier, options, callback);
  }
}

export default DateControlledCollectionMixin;