const methods = {};

const ReactBridge = {
  define(name, functionRef) {
    if (typeof methods[name] !== 'undefined') {
      throw new Error(`Bridge '${name}' already defined`);
    }
    
    if (typeof functionRef !== 'function') {
      throw new Error('Only functions can be provided');
    }
    
    methods[name] = functionRef;
  },
  
  remove(name) {
    delete methods[name];
  },
  
  isDefined(name) {
    return typeof methods[name] !== 'undefined';
  },
  
  call(name, ...parameters) {
    if (typeof methods[name] === 'undefined') {
      throw new Error(`Bridge '${name}' not defined`);
    }
    
    return methods[name](...parameters);
  }
};

export default ReactBridge;