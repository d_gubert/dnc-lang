import AppCollection from '/imports/lib/collection/AppCollection.js';
import ClassSchema from './schema.js';

import Students from '/imports/api/students/students.js';

import moment from 'moment';
import 'twix';

class ClassCollection extends AppCollection {
  constructor(collectionName, schema) {
    const transform = function(classDoc) {
      classDoc.student = Students.findOne({_id: classDoc.studentId});

      if (classDoc.student) {
        classDoc.studentName = classDoc.student.name;
      }

      classDoc.startMoment = moment(classDoc.startDate);
      classDoc.endMoment   = moment(classDoc.endDate);

      classDoc.range = moment(classDoc.startDate).twix(classDoc.endDate);

      classDoc.date  = classDoc.startMoment.format('YYYY-MM-DD');

      classDoc.start = classDoc.startMoment.format('HH:mm');
      classDoc.end   = classDoc.endMoment.format('HH:mm');

      return classDoc;
    };

    super(collectionName, schema, {transform});
  }
}

const Classes = new ClassCollection('classes', ClassSchema);

export default Classes;
