/* global SimpleSchema */

ClientSchema = new SimpleSchema({
  name: {
    type: String,
    max: 200
  },
  email: {
    type: String,
    regEx: SimpleSchema.RegEx.Email
  },
  hourValue: {
    type: Number
  },
  createdAt: {
    type: Date
  },
  updatedAt: {
    type: Date,
    optional: true
  },
  active: {
    type: Boolean,
    defaultValue: true
  },
  hourValueHistory: {
    type: [Object],
    defaultValue: []
  },
  'hourValueHistory.$.value': {
    type: Number
  },
  'hourValueHistory.$.date': {
    type: Date
  },
  teacherId: {
    type: String,
    regEx: SimpleSchema.RegEx.Id
  },
  phone: {
    type: Number,
    optional: true
  },
});