import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

import React, { Component } from 'react';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import Snackbar from './components/common/Snackbar.jsx';
import ConfirmDialog from './components/common/ConfirmDialog.jsx';

import ReactBridge from '/imports/lib/ui/ReactBridge.js';

export default class App extends Component {
  componentWillMount() {
    Meteor.subscribe('Students.ListActive');
    Meteor.subscribe('Clients.ListActive');
    Meteor.subscribe('Classes.ListValid');

    Tracker.autorun(() => {
      this.checkIfUserIsLoggedIn();
    });
  }

  componentDidUpdate() {
    this.checkIfUserIsLoggedIn();
    ReactBridge.call('LeftNav.hide');
  }

  checkIfUserIsLoggedIn() {
    if (!Meteor.userId() && !this.context.router.isActive('Login')) {
      this.context.router.push('/login');
    }
  }

  render() {
    return (
      <MuiThemeProvider muiTheme={getMuiTheme()}>
        <section>
          {this.props.children}
          <Snackbar />
          <ConfirmDialog />
        </section>
      </MuiThemeProvider>
    );
  }
}

App.contextTypes = {
  router: React.PropTypes.object.isRequired
};
