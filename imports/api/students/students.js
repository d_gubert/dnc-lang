import { Meteor } from 'meteor/meteor';

import AppCollection from '/imports/lib/collection/AppCollection.js';
import StudentSchema from './schema.js';

import Clients from '/imports/api/clients/clients.js';

class StudentsCollection extends AppCollection {
  findStudentWithClientInfo(studentId) {
    const student = this.findOne(studentId);

    if (student && student.clientId) {
      student.clientInfo = Clients.findOne(student.clientId);
    }

    return student;
  }
}

const Students = new StudentsCollection('students', StudentSchema);

export default Students;